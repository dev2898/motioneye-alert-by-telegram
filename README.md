# Motioneye alert by Telegram

Un simple script python pour m'envoyer via Telegram les alertes de la video-surveillance Motioneye

Lancer le script via l'option "Run An End Command" de Motioneye dans les paramèteres de "Motion Notifications"
```
sudo python3 /home/alert_motioneye.py CAMERA_PATH
```

## To Do

- Faire évoluer subprocess.check_output() qui n'est pas très "smart"  (Entre autres idiotie, il impose une vidéo en mp4 )
