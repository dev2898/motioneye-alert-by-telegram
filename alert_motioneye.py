#!/usr/bin/env python3

# Auteur : Vincent Brevard
# Date : Septembre 2023
# Pourquoi : Se fait déclencher par montioneye après une détection
#            Récupère la vidéo de la détection motioneye et l'envoi via Telegram

import telepot
import os
import sys
import subprocess
from datetime import datetime

#Renseignez ici votre token telegram
TOKEN = 'xxxxxxxxxxxxx'
CHATID = 'xxxxxxxxxxxxx'

def date():
        # Obtenir la date actuelle
        date_actuelle = datetime.now()
        # Formater la date au format "YYYY-MM-DD"
        date_formatee = date_actuelle.strftime("%Y-%m-%d")
        return date_formatee

def init_bot():
        bot = telepot.Bot(TOKEN)
        return bot

def main(PATH, bot):
        CamName = PATH.split('/')[-2]
        bot.sendMessage(CHATID, 'MotionEye a détecté un mouvement : ' + CamName)

        dday = date()

        result = subprocess.check_output('ls -lrth ' + PATH + dday + '/ | grep mp4 | grep -v .thumb | awk \'{print $9}\' | tail -n 1', shell = True)
        video=result.decode('UTF-8')
        if not video:
                bot.sendMessage(CHATID, 'alert_motioneye : error video')
        else:
                bot.sendVideo(CHATID, open(PATH + dday + '/' + video.strip(), 'rb'), supports_streaming=True)
                bot.sendMessage(CHATID, video)

if __name__ == '__main__':

        bot = init_bot()
        if len(sys.argv) == 1:
                bot.sendMessage(CHATID, 'alert_motioneye : error param')
        else:
                main(sys.argv[1], bot)
